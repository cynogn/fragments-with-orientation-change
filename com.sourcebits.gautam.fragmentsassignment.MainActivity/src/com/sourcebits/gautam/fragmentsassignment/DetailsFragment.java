package com.sourcebits.gautam.fragmentsassignment;

import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Details Fragment displays the details of device
 * 
 * @author Gautam
 * 
 */
public class DetailsFragment extends ListFragment {

	private static final String TAG = DetailsFragment.class.getCanonicalName();
	/**
	 * index of the product
	 */
	private int mProductDetails;
	public int pos;

	
	/**
	 * Constructor of DetailsFragment+
	 */
	public DetailsFragment() {
	}

	/**
	 * Sets the index of the previously clicked ProductFragment's list
	 */
	public void setPos(int pos) {
		mProductDetails = pos;
		
	}

	/**
	 * On create
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate()");
		super.onCreate(savedInstanceState);
	}

	/**
	 * Create View, where the UI is rendered
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//savedInstanceState.putInt("screenno", 3);
		Log.d(TAG, "onCreateView()");
		CustomAdapter customAdapter = null;
		switch (mProductDetails) {
		/**
		 * if postion is 0 displays the mac book config
		 */
		case 0:
			customAdapter = new CustomAdapter(getActivity(), getResources()
					.getStringArray(R.array.macbook));
			break;
		/**
		 * if postion is 1 displays the iMac's config
		 */
		case 1:
			customAdapter = new CustomAdapter(getActivity(), getResources()
					.getStringArray(R.array.imacbook));
			break;

		/**
		 * if postion is 2 displays the iPhone's book config
		 */
		case 2:
			customAdapter = new CustomAdapter(getActivity(), getResources()
					.getStringArray(R.array.iPhonecondif));

			break;
		// TODO
		case 3:
			customAdapter = new CustomAdapter(getActivity(), getResources()
					.getStringArray(R.array.macbook));

			break;
		// TODO
		case 4:
			customAdapter = new CustomAdapter(getActivity(), getResources()
					.getStringArray(R.array.macbook));

			break;
		}

		setListAdapter(customAdapter);
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}
	public void onConfigurationChanged(Configuration newConfig) {
		ProductFragment details = new ProductFragment();
		super.onConfigurationChanged(newConfig);

	}
}