package com.sourcebits.gautam.fragmentsassignment;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;

/**
 * Main Activity start screens which loads the manufactue's List
 * 
 * @author Gautam
 * 
 */
public class MainActivity extends Activity {
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		setContentView(R.layout.activity_main);
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_main);
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
			android.app.FragmentManager fragmentManager = getFragmentManager();
			FragmentTransaction fragmentTransaction = fragmentManager
					.beginTransaction();
			IndexFragment deviceManufactures = new IndexFragment();
			fragmentTransaction.add(R.id.fragment_container0,
					deviceManufactures, "deviceManufactures");
			Log.v("1", "portrait");
			fragmentTransaction.commit();
		}
		setContentView(R.layout.activity_main);
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
			android.app.FragmentManager fragmentManager = getFragmentManager();
			FragmentTransaction fragmentTransaction = fragmentManager
					.beginTransaction();
			IndexFragment deviceManufactures = new IndexFragment();
			fragmentTransaction.add(R.id.fragment_container0,
					deviceManufactures, "deviceManufactures");
			Log.v("1", "landscape");
			fragmentTransaction.commit();
		}
	}

}
