package com.sourcebits.gautam.fragmentsassignment;

import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

/**
 * IndexFragament displays the device Manufactures list
 * 
 * @author Gautam B
 * 
 */
public class IndexFragment extends ListFragment {
	private static final String TAG = IndexFragment.class.getCanonicalName();
	public int pos;

	/**
	 * Constructor of the Index fragment
	 */
	public IndexFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate()");
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView()");
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	/**
	 * set the manufactures list from the string.xml
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		CustomAdapter customAdapter = new CustomAdapter(getActivity(),
				getResources().getStringArray(R.array.manufactures));
		setListAdapter(customAdapter);
	}

	/**
	 * On click of the list item it populates a new list
	 */
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Log.d("manu", "pos: " + position);

		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
			ProductFragment details = new ProductFragment();
			details.setPos(position);
			FragmentTransaction fTrans = getFragmentManager()
					.beginTransaction();

			fTrans.replace(R.id.fragment_container0, details);

			Log.v("2", "portrait");
			fTrans.commit();
		} else {

			ProductFragment details = new ProductFragment();
			details.setPos(position);
			pos = position;
			FragmentTransaction fTrans = getFragmentManager()
					.beginTransaction();
			fTrans.replace(R.id.fragment_container2, details);
			Log.v("2", "landscape");
			fTrans.commit();
		}

	}

	/*public void onConfigurationChanged(Configuration newConfig) {
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
			ProductFragment details1 = new ProductFragment();

			details1.setPos(pos);
			FragmentTransaction fTrans = getFragmentManager()
					.beginTransaction();
			fTrans.replace(R.id.fragment_container0, details1);
			Log.v("2", "portrait");
			fTrans.commit();
		} else {
			ProductFragment details1 = new ProductFragment();

			details1.setPos(pos);
			FragmentTransaction fTrans = getFragmentManager()
					.beginTransaction();
			fTrans.replace(R.id.fragment_container2, details1);
			Log.v("2", "landscape");
			fTrans.commit();
		}
		super.onConfigurationChanged(newConfig);

	}*/
}
